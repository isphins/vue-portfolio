const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: pkg.description
      }
    ],
    script: [{
        src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js'
      },
      {
        src: "https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"
      }, {
        src: "js/jquery-panelslider/jquery.panelslider.min.js"
      },
      {
        src: "js/fresh.js"
      }
    ],
    link: [{
        rel: 'icon',
        type: 'image/png',
        href: 'favicon.png'
      },
      {
        rel: 'stylesheet',
        href: 'css/bulma.min.css'
      },
      {
        rel: 'stylesheet',
        href: 'css/core.css'
      },
      {
        rel: 'stylesheet',
        href: 'css/icons.css'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Source+Code+Pro:400,700'
      }
    ]
  },
  // rel="icon" type="image/png" href="assets/images/favicon.png"
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#F39200'
  },

  /*
   ** Global CSS
   */
  css: [],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios'
  ],

  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {

    }
  }
}
